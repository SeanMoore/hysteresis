#include "ab_plane.h"

ABPlane::ABPlane( int dimension )
    : dimension_( dimension )
    , input_( 0.0 )
    , output_( 0.0 ) {
    domain_state_.resize( dimension_ * dimension_ );
    domain_magnitude_.resize( dimension_ * dimension_ );

    for( auto& i : domain_state_ ) {
        i = false;
    }
    for( int alpha = 0; alpha < dimension_; alpha++ ) {
        for( int beta = 0; beta < dimension_; beta++ ) {
            domain_magnitude_[alpha * dimension_ + beta] = ( alpha <= beta ) ? 1.0 : 0.0;
        }
    }

    SetInput( 0.0 );
    history_input_.append( 0.0 );
    history_output_.append( 0.0 );
}

ABPlane::~ABPlane() {}

int ABPlane::GetDimension() const {
    return dimension_;
}

double ABPlane::GetInput() const {
    return input_;
}

double ABPlane::GetOutput() const {
    return output_;
}

int ABPlane::GetHistoryLength() const {
    return history_input_.length();
}

double ABPlane::getHistoryInput( int i ) const {
    if( i < 0 ) {
        return 0.0;
    }
    if( history_input_.length() <= i ) {
        return 0.0;
    }
    return history_input_[i];
}

double ABPlane::getHistoryOutput( int i ) const {
    if( i < 0 ) {
        return 0.0;
    }
    if( history_output_.length() <= i ) {
        return 0.0;
    }
    return history_output_[i];
}

double ABPlane::getDomain( int alpha, int beta ) const {
    if( ( alpha < 0 ) ||
        ( dimension_ <= alpha ) ||
        ( beta < 0 ) ||
        ( dimension_ <= beta ) ) {
        return 0.0;
    }

    return domain_state_[alpha * dimension_ + beta] *
           domain_magnitude_[alpha * dimension_ + beta];
}

void ABPlane::SetInput( double input ) {
    input_       = input;
    output_      = 0.0;
    double total = 0.0;
    for( int alpha = 0; alpha < dimension_; alpha++ ) {
        for( int beta = 0; beta < dimension_; beta++ ) {
            double alpha_threshold = static_cast<double>( alpha ) / static_cast<double>( dimension_ );
            double beta_threshold  = static_cast<double>( beta + 1 ) / static_cast<double>( dimension_ );

            auto& pixel = domain_state_[alpha * dimension_ + beta];
            if( input_ <= alpha_threshold ) {
                pixel = false;
            } else if( beta_threshold <= input_ ) {
                pixel = true;
            }
            double magnitude = domain_magnitude_[alpha * dimension_ + beta];
            output_ += pixel * magnitude;
            total += magnitude;
        }
    }

    output_ /= total;
    history_input_.append( input_ );
    history_output_.append( output_ );

    emit outputChanged();
    emit historyLengthChanged();
}
