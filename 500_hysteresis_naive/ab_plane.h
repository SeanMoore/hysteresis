#ifndef AB_PLANE_H
#define AB_PLANE_H

#include <QObject>

class ABPlane : public QObject {
    Q_OBJECT

    Q_PROPERTY( int dimension READ GetDimension NOTIFY DimensionChanged )
    Q_PROPERTY( double input READ GetInput WRITE SetInput NOTIFY InputChanged )
    Q_PROPERTY( double output READ GetOutput NOTIFY outputChanged )
    Q_PROPERTY( int historyLength READ GetHistoryLength NOTIFY historyLengthChanged )
    Q_PROPERTY( QVector<double> historyInput NOTIFY historyLengthChanged )
    Q_PROPERTY( QVector<double> historyOutput NOTIFY historyLengthChanged )

public:
    ABPlane( int dimension );
    virtual ~ABPlane();

    int    GetDimension() const;
    double GetInput() const;
    double GetOutput() const;

public slots:
    int    GetHistoryLength() const;
    double getHistoryInput( int i ) const;  // QML
    double getHistoryOutput( int i ) const; // QML
    double getDomain( int alpha, int beta ) const;
    void   SetInput( double input );

signals:
    void DimensionChanged();
    void InputChanged();
    void outputChanged(); // QML
    void historyLengthChanged();

private:
    int             dimension_;
    double          input_;
    double          output_;
    QVector<bool>   domain_state_;
    QVector<double> domain_magnitude_;
    QVector<double> history_input_;
    QVector<double> history_output_;
};

#endif // AB_PLANE_H
