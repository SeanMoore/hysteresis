import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.12

Item {
    anchors.fill: parent

    RowLayout {
        anchors.fill: parent

        Rectangle{
            id: domain
            color: "black"
            Layout.fillHeight: true
            Layout.preferredWidth: height

            Canvas {
                id: graph
                anchors.fill: parent
                onPaint: {
                    var ctx = getContext("2d");
                    ctx.clearRect( 0, 0, domain.width, domain.height )
                    ctx.beginPath();
                    ctx.strokeStyle = 'rgb(0, 255, 0)'
                    ctx.moveTo(abPlane.getHistoryInput(0), abPlane.getHistoryOutput(0))
                    for( var i=1; i<abPlane.historyLength; ++i ) {
                        ctx.lineTo(domain.width*abPlane.getHistoryInput(i), domain.height*abPlane.getHistoryOutput(i))
                    }
                    ctx.stroke();
                }
                Connections {
                    target: abPlane
                    function onHistoryLengthChanged() {
                        graph.requestPaint()
                    }
                }
            }
        }

        ColumnLayout {
            Layout.margins: 10
            Layout.fillWidth: true
            Layout.fillHeight: true

            RowLayout {
                Label { text: "Input" }
                Label { id: inputValue }
            }
            Slider {
                from: 0
                to: 1
                value: 0.0
                onValueChanged: {
                    inputValue.text = value.toFixed(3)
                    abPlane.input = value
                }
            }
            RowLayout {
                Label { text: "Output" }
                Label {
                    id: outputValue
                    text: abPlane.output.toFixed(3)
                }
            }
        }
    }
}
