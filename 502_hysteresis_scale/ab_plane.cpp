#include "ab_plane.h"

ABPlane::ABPlane( int dimension )
    : dimension_( dimension )
    , input_( 0.0 )
    , output_( 0.0 ) {
    for( int i = 0; i < dimension_; ++i ) {
        domain_state_diagonal_.append( 0.0 );
    }

    SetInput( 0.0 );
    history_input_.append( 0.0 );
    history_output_.append( 0.0 );
}

ABPlane::~ABPlane() {}

int ABPlane::GetDimension() const {
    return dimension_;
}

double ABPlane::GetInput() const {
    return input_;
}

double ABPlane::GetOutput() const {
    return output_;
}

int ABPlane::GetHistoryLength() const {
    return history_input_.length();
}

double ABPlane::getHistoryInput( int i ) const {
    if( i < 0 ) {
        return 0.0;
    }
    if( history_input_.length() <= i ) {
        return 0.0;
    }
    return history_input_[i];
}

double ABPlane::getHistoryOutput( int i ) const {
    if( i < 0 ) {
        return 0.0;
    }
    if( history_output_.length() <= i ) {
        return 0.0;
    }
    return history_output_[i];
}

double ABPlane::getDomain( int alpha, int beta ) const {
    if( ( alpha < 0 ) ||
        ( dimension_ <= alpha ) ||
        ( beta < 0 ) ||
        ( dimension_ <= beta ) ||
        ( beta < alpha ) ) {
        return 0.0;
    }

    return ( alpha < domain_state_diagonal_[beta - alpha] );
}

void ABPlane::SetInput( double input ) {
    int input_int = static_cast<int>( input * dimension_ );
    input_        = input;
    output_       = 0.0;
    for( int beta = 0; beta < dimension_; beta++ ) {
        int& diagonal       = domain_state_diagonal_[beta];
        int  diagonal_alpha = diagonal;
        int  diagonal_beta  = diagonal + beta;
        if( input_int <= diagonal_alpha ) {
            diagonal = input_int;
        } else if( diagonal_beta <= input_int ) {
            diagonal = input_int - beta;
        }
        output_ += diagonal; // Assuming all domains are magnitude 1.
    }

    output_ = ( 2 * output_ ) / ( dimension_ * ( dimension_ + 1 ) );
    history_input_.append( input_ );
    history_output_.append( output_ );

    emit outputChanged();
    emit historyLengthChanged();
}
