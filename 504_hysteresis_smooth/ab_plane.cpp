#include "ab_plane.h"

#include <QtMath>

namespace {
    double HyperTriangleNumber( uint64_t dimension, uint64_t size ) {
        double denominator = 1;
        double numerator   = 1;
        for( uint64_t i = 0; i < dimension; ++i ) {
            denominator = denominator * ( 1 + i );
            numerator   = numerator * ( size + i );
        }
        return static_cast<double>( numerator ) / denominator;
    }
} // namespace

Trough::Trough( double k, double c, double d, double l )
    : k_( k )
    , c_( c )
    , l_( l )
    , tau_d_( 2 * M_PI * d )
    , pscale_( 4 * qPow( k, 2 ) ) {}

double Trough::f( double x, double a, double b ) {
    double offset    = -2 * k_ * a;
    double parabolic = qPow( g( x, a, b ) + offset, 2 ) / pscale_;
    double sine      = ( qCos( tau_d_ * x ) + 1 ) / ( 2 * qPow( tau_d_, 2 ) );
    return l_ * ( c_ * parabolic + ( 1 - c_ ) * sine );
}
double Trough::df( double x, double a, double b ) {
    double offset    = -2 * k_ * a;
    double parabolic = ( g( x, a, b ) + offset ) * dg( x, a, b ) / pscale_;
    double sine      = -qSin( tau_d_ * x ) / tau_d_;
    return l_ * ( c_ * parabolic + ( 1 - c_ ) * sine );
}
double Trough::ddf( double x, double a, double b ) {
    double offset    = -2 * k_ * a;
    double parabolic = ( ( g( x, a, b ) + offset ) * ddg( x, a, b ) + qPow( dg( x, a, b ), 2 ) ) / pscale_;
    double sine      = -qCos( tau_d_ * x );
    return l_ * ( c_ * parabolic + ( 1 - c_ ) * sine );
}

double Trough::g( double x, double a, double b ) {
    return h( x, a, b, 1 ) + h( x, a, b, -1 );
}
double Trough::dg( double x, double a, double b ) {
    return dh( x, a, b, 1 ) + dh( x, a, b, -1 );
}
double Trough::ddg( double x, double a, double b ) {
    return ddh( x, a, b, 1 ) + ddh( x, a, b, -1 );
}

double Trough::h( double x, double a, double b, double s ) {
    return qSqrt( 1 + qPow( k_ * ( x - s * a - b ), 2 ) );
}
double Trough::dh( double x, double a, double b, double s ) {
    return ( qPow( k_, 2 ) * ( x - s * a - b ) ) / qSqrt( 1 + qPow( k_ * ( x - s * a - b ), 2 ) );
}
double Trough::ddh( double x, double a, double b, double s ) {
    return qPow( k_, 2 ) / qPow( 1 + qPow( k_ * ( x - s * a - b ), 2 ), 3.0 / 2.0 );
}

ABPlane::ABPlane( int dimension )
    : dimension_( dimension )
    , input_( 0.0 )
    , output_( 0.0 )
    , timestep_ms_( 10 )
    , trough_( 100, 0.9, 10, 1.0 ) {
    for( int i = 0; i < dimension_; ++i ) {
        domain_state_diagonal_.append( 0.0 );
    }

    SetInput( 0.0 );
    history_input_.append( 0.0 );
    history_output_.append( 0.0 );
}

ABPlane::~ABPlane() {}

int ABPlane::GetDimension() const {
    return dimension_;
}

double ABPlane::GetInput() const {
    return input_;
}

double ABPlane::GetOutput() const {
    return output_;
}

int ABPlane::GetTimestepMs() const {
    return timestep_ms_;
}

int ABPlane::GetHistoryLength() const {
    return history_input_.length();
}

double ABPlane::getHistoryInput( int i ) const {
    if( i < 0 ) {
        return 0.0;
    }
    if( history_input_.length() <= i ) {
        return 0.0;
    }
    return history_input_[i];
}

double ABPlane::getHistoryOutput( int i ) const {
    if( i < 0 ) {
        return 0.0;
    }
    if( history_output_.length() <= i ) {
        return 0.0;
    }
    return history_output_[i];
}

double ABPlane::getDomain( int alpha, int beta ) const {
    if( ( alpha < 0 ) ||
        ( dimension_ <= alpha ) ||
        ( beta < 0 ) ||
        ( dimension_ <= beta ) ||
        ( beta < alpha ) ) {
        return 0.0;
    }

    return ( alpha < domain_state_diagonal_[beta - alpha] );
}

int ABPlane::GetDiagonalLength() const {
    return domain_state_diagonal_.size();
}

double ABPlane::getDiagonal( int beta ) const {
    if( ( beta < 0 ) ||
        ( dimension_ <= beta ) ) {
        return 0.0;
    }

    return domain_state_diagonal_[beta];
}

void ABPlane::SetInput( double input ) {
    input_ = input;

    history_input_.append( input_ );
    history_output_.append( output_ );

    step();

    emit historyLengthChanged();
}

void ABPlane::step() {
    uint64_t bend = 5;
    output_       = 0;
    for( int beta = 0; beta < dimension_; beta++ ) {
        double&      x = domain_state_diagonal_[beta];
        const double a = beta / 2.0;
        const double b = ( input_ * dimension_ ) - a;
        x -= trough_.df( x, a, b );
        output_ += ( x * HyperTriangleNumber( bend - 2, beta + 1 ) );
    }
    output_                = output_ / HyperTriangleNumber( bend, dimension_ );
    history_output_.last() = output_;
    emit outputChanged();
}

void ABPlane::SetTimestepMs( int timestep_ms ) {
    timestep_ms_ = timestep_ms;
    emit TimestepMsChanged();
}
