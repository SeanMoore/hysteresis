#ifndef AB_PLANE_H
#define AB_PLANE_H

#include <QObject>

class Trough {
public:
    Trough( double k, double c, double d, double l );

    double f( double x, double a, double b );
    double df( double x, double a, double b );
    double ddf( double x, double a, double b );

    double g( double x, double a, double b );
    double dg( double x, double a, double b );
    double ddg( double x, double a, double b );

    double h( double x, double a, double b, double s );
    double dh( double x, double a, double b, double s );
    double ddh( double x, double a, double b, double s );

private:
    double k_;
    double c_;
    double l_;

    double tau_d_;
    double pscale_;
};

class ABPlane : public QObject {
    Q_OBJECT

    Q_PROPERTY( int dimension READ GetDimension NOTIFY DimensionChanged )
    Q_PROPERTY( double input READ GetInput WRITE SetInput NOTIFY InputChanged )
    Q_PROPERTY( double output READ GetOutput NOTIFY outputChanged )
    Q_PROPERTY( int historyLength READ GetHistoryLength NOTIFY historyLengthChanged )
    Q_PROPERTY( QList<double> historyInput NOTIFY historyLengthChanged )
    Q_PROPERTY( QVector<double> historyOutput NOTIFY historyLengthChanged )
    Q_PROPERTY( int diagonalLength READ GetDiagonalLength NOTIFY historyLengthChanged )
    Q_PROPERTY( QVector<double> diagonal NOTIFY historyLengthChanged )
    Q_PROPERTY( int timestepMs READ GetTimestepMs WRITE SetTimestepMs NOTIFY TimestepMsChanged )

public:
    ABPlane( int dimension );
    virtual ~ABPlane();

    int    GetDimension() const;
    double GetInput() const;
    double GetOutput() const;
    int    GetTimestepMs() const;

public slots:
    int    GetHistoryLength() const;
    double getHistoryInput( int i ) const;  // QML
    double getHistoryOutput( int i ) const; // QML
    double getDomain( int alpha, int beta ) const;
    int    GetDiagonalLength() const;     // QML
    double getDiagonal( int beta ) const; // QML
    void   SetInput( double input );
    void   step(); // QML
    void   SetTimestepMs( int timestep_ms );

signals:
    void DimensionChanged();
    void InputChanged();
    void outputChanged(); // QML
    void historyLengthChanged();
    void TimestepMsChanged();

private:
    int           dimension_;
    double        input_;
    double        output_;
    QList<double> domain_state_diagonal_;
    QList<double> history_input_;
    QList<double> history_output_;
    int           timestep_ms_;
    Trough        trough_;
};

#endif // AB_PLANE_H
