#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>

#include "ab_plane.h"

int main( int argc, char* argv[] ) {
#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
#endif

    QGuiApplication app( argc, argv );

    ABPlane    abPlane( 1024 );
    QQuickView view;
    view.rootContext()->setContextProperty( "abPlane", &abPlane );
    view.setSource( QUrl( "qrc:///main.qml" ) );
    view.show();

    return app.exec();
}
